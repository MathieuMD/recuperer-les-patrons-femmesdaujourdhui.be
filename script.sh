#!/bin/bash
######################################################################
# Description:
#   Récupère tous les patrons offerts par Femmes d'aujourd'hui, en essayant de
#   limiter la charge sur leurs serveurs (Même s'ils sont chez Cloudflare).
#   https://www.femmesdaujourdhui.be/patronscouture/
######################################################################
set -e
TMP="$(mktemp)"
OUTPUT="$(basename $0).output"
exec > >(tee $OUTPUT) 2>&1

# On met tout dans le sous-dossier patrons
BASE="$(pwd)/patrons"
if [ ! -d "$BASE" ]; then
    mkdir "$BASE"
fi
cd "$BASE"

# Récupération des pages d'index
CACHEEXP="$(($(date +%s)-24*3600))"
echo -en "Index:\t"
# On ne les télécharge que sa la copie locale n'existe pas ou a plus de 24h
if [ $(stat --format=%Y index.html 2>/dev/null ||echo 0) -lt $CACHEEXP ]; then
    curl -s -o index.html https://www.femmesdaujourdhui.be/patronscouture/
    echo "downloaded"
    sleep 0.3
else
    echo "incache"
fi
PAGES=$(grep /page/ index.html |cut -d: -f2 |cut -d/ -f6 |sort |tail -1)
for PAGE in $(seq 2 $PAGES); do
    echo -en "Index $PAGE:\t"
    if [ $(stat --format=%Y index$PAGE.html 2>/dev/null ||echo 0) -lt $CACHEEXP ]; then
        curl -s -o index$PAGE.html https://www.femmesdaujourdhui.be/patronscouture/page/$PAGE/
        echo "downloaded"
        sleep 0.3
    else
        echo "incache"
    fi
done

# Récupération récursive des pages, des PDF et de l'image de chaque patrons.
# Mais on ne les récupère pas à nouveau si on les a déjà en local.
for INDEX in index*.html; do
    grep c-teaser-block__visual-link $INDEX \
        |cut -d/ -f5 \
        |while read NAME; do
            cd "$BASE"

            # Page HTML
            echo -en "${NAME}:\t"
            if [ ! -d "$NAME" ]; then
                mkdir "$NAME"
            fi
            cd "$NAME"
            HTML="$NAME.html"
            if [ ! -s "$HTML" ]; then
                curl -s -o $HTML https://www.femmesdaujourdhui.be/patronscouture/$NAME/
                sleep 0.3
            fi

            # Thumbnail
            THUMBURL="$(grep 'property="og:image"' $HTML |head -1 |cut -d'"' -f4)"
            THUMBNAME="$(basename "$THUMBURL")"
            echo -n "$THUMBNAME:"
            if [ -s "$THUMBNAME" ]; then
                echo -n "incache"
            else
                curl -s -O "$THUMBURL"
                echo -n "downloaded"
                echo "$THUMBNAME" >> "$TMP"
                sleep 0.3
            fi
            echo -en "\t"

            # PDF
            # Leur description n'est pas stable (par ex. "Description de
            # travail", mais pas toujours), donc on récupère tous les PDF, sauf
            # les A1 (qui sont nommés ainsi : wizzy-a1-fr-pdf.pdf).
            # 2025-02-11 "cut -f6" → "-f10"
            grep -i \\.pdf $HTML \
                |cut -d'"' -f10 \
                |grep -v -- '-a1-' \
                |sort -u \
                |while read PDF; do
                    PDFNAME="$(basename "$PDF")"
                    echo -n "$PDFNAME:"
                    if [ -s "$PDFNAME" ]; then
                        echo -n "incache"
                    else
                        curl -s -O $PDF
                        echo -n "downloaded"
                        sleep 0.3
                    fi
                    echo -en "\t"
                done

            echo "Done."
        done
done
#done | tee -a $OUTPUT

# Création d'images d'index avec le nom du patron
cd "$BASE"
echo -n "Montage of thumbnails... "
# Uniquement si on a téléchargé au moins un JPG
if [ -s $TMP ]; then
    # Vue la quantité d'images, il peut falloir modifier
    # /etc/ImageMagick-6/policy.xml :
    #   <policy domain="resource" name="disk" value="4GiB"/>
    montage -mode concatenate -tile 4x3 -resize 250x250 -label "%d" \
        */*.jpg ../index.jpg
    echo "Done."
else
    echo "Not needed."
fi
rm $TMP
